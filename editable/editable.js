var listeModif = [];
var triEnCours = "aucun";
var addNewLineForm = false;
var listeInputs = [];


// ---------------------- la big fonction
async function getDatas(url) {

    const response = await fetch(url);
    if (!response.ok && response.status === 404) {
        return console.log("Erreur 404, l'url de votre fichier est invalide");
    }
    else if (!response.ok) {
        return console.log("URL valide mais il y a eu un problème, verifiez de votre coté")
    }
    var tableau = document.getElementById('tableau');
    while (tableau.firstChild) {
        tableau.firstChild.remove()
    }
    var data = await response.json();
    var x = data[0];
    var colonnes = Object.keys(x);

    var listeModif = {};
    var lignesARajouter = 0;
    for ( let id of Object.keys(sessionStorage) ) {
        var valeur = sessionStorage.getItem(id);
        listeModif[id] = valeur; 
    }
    for (let x of Object.keys(listeModif)) {
        var chemin = x.split(',');
        var col = chemin[0];
        var lig = chemin[1]; 
        console.log("nb colonnes : " + colonnes.length + " ligne val a modif : " + lig + " colonnes.length - lig - 1 : " + colonnes.length - lig - 1);
        if ( (lig - data.length + 1) > lignesARajouter)  {
            lignesARajouter = (lig - data.length + 1);
        }
    }

    for (let i=0; i<lignesARajouter; i++) {
        var ligneVide = {};
        console.log(lignesARajouter);
        console.log(colonnes.length);
        for (let j = 0; j < colonnes.length; j++) {
            ligneVide[colonnes[j]] = 1;
        }
        data.push(ligneVide);
    } 

    for (let x of Object.keys(listeModif)) {
        var chemin = x.split(',');
        var col = chemin[0];
        var lig = chemin[1]; 
        data[lig][col] = listeModif[x];
    }



    // ---------------------- Ajout d'une nouvelle ligne dans data
    if (addNewLineForm) {
        var inputsNewLine = document.getElementsByClassName("addNewLine");
        var listeInputs = [];
        for (var i = 0; i < inputsNewLine.length; i++) {
            listeInputs.push(inputsNewLine[i].value);
        }

        var newInfos = {};
        for (var i = 0; i < colonnes.length; i++) {
            var date = data[0][colonnes[i]].toString().split(' ').join('');

            if (typeof data[0][colonnes[i]] == 'boolean') {
                listeInputs[i] = Boolean(listeInputs[i]);
            }

            else if (Array.isArray(data[0][colonnes[i]])) {
                listeInputs[i] = listeInputs[i].split(",")
            }

            else if (Date.parse(date)) {
                //listeInputs[i] = new Date(date);
            }

            else {

            }
            newInfos[colonnes[i]] = listeInputs[i];
            id = colonnes[i] + "," + data.length.toString()
            sessionStorage.setItem(id, listeInputs[i]);
        }
        data.push(newInfos);
        addNewLineForm = false;
        // rajouter newInfos dans liste modif
    }

    // ---------------------- Changements des données suivant le type pour le tri

    // ---------------------- Tri du tableau
    function trier(tableau, attr) {
        if (typeof data[0][attr] == 'boolean') {
            return tableau.sort((a, b) => { return (b[attr] - a[attr]); })
        }

        else if (Array.isArray(data[0][attr])) {
            // on classe les tableaux par nombre d'elements croissant
            return tableau.sort((a, b) => { return (a[attr].length - b[attr].length); })
        }

        else if (typeof data[0][attr] == 'number' || 'string') {
            return tableau.sort((a, b) => { return ((a[attr] > b[attr]) - (a[attr] < b[attr])); })
        }
        // si le type est "inconnu" on ne fait rien
        else {
            return tableau;
        }
    }

    if (triEnCours != "aucun") {
        data = trier(data, triEnCours);
    }


    // ---------------------- Creation des colonnes
    var header = document.createElement('thead');
    header.setAttribute('id', "header");

    var i = 0;
    var row = document.createElement('tr');
    row.setAttribute('id', "ligneHeader");
    for (const colonne of colonnes) { // const car "Pensez au fait que les clefs dans l'entête du tableau ne sont pas éditable !"

        var infosCellule = {
            id: "colonne" + "," + i.toString(),
            content: colonne
        };

        var cellule = document.createElement('th');
        cellule.setAttribute('id', infosCellule.id);
        cellule.innerText = infosCellule.content;
        cellule.classList.add('celluleHeader');
        cellule.addEventListener("click", tri);
        row.appendChild(cellule);
        i++;
    }
    header.appendChild(row);
    tableau.appendChild(header);


    var tableBody = document.createElement('tbody');

    // ---------------------- Creations des lignes

    for (var numLigne = 0; numLigne < data.length; numLigne++ ) {
        var ligne = data[numLigne];
        var maLigne = document.createElement('tr');
        maLigne.setAttribute('id', "ligne" + "," + numLigne.toString());
        maLigne.classList.add('ligne');




        function checkImage(url) {
            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.send();
            request.onload = function () {
                if (request.status == 200) //if(statusText == OK)
                {
                    return true;

                }
                else {
                    return false;
                }
            }
        }

        
        for (var colonne of colonnes) {

            var infosCellule = {
                id: colonne + "," + numLigne.toString(),
                content: ligne[colonne]
            };
            
            

            var imageErrorTest = 0;
            var cellule = document.createElement('th');
            var regexurl = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
            var regex = new RegExp(regexurl);

            var date = false;
            var date = infosCellule.content.toString().split(' ').join('');

            if (Array.isArray(infosCellule.content)) {
                // on l'affiche en tableau ?
                var maListe = infosCellule.content;
                var ul = document.createElement('ul');
                for (elmt of maListe) {
                    var li = document.createElement('li');
                    li.innerText = elmt;
                    ul.appendChild(li);
                }
                cellule.innerText = ""
                ul.classList.add('liste');
                cellule.appendChild(ul);
            }

            else if (infosCellule.content.toString().match(regex)) {
                // si la donnée correspond à une URL en regex, on verifie si le lien est valide via HTTP
                // on autorise par défaut l'image de person.json pour contourner l'accés non autorisé
                if (checkImage(infosCellule.content) || infosCellule.content == "http://placehold.it/32x32") {
                    var image = new Image();
                    image.src = infosCellule.content;
                    // on verifie que c'est bien une image affichable
                    if ((image.width > 0) && (image.height > 0)) {
                        cellule.innerText = "";
                        image.classList.add('img');
                        cellule.appendChild(image);
                    }
                }
            }


            else if (Date.parse(date)) {
                realDate = new Date(date);
                cellule.innerText = "Année : " + realDate.getFullYear() +
                    "\n Mois : " + realDate.getMonth() +
                    "\n Jour : " + realDate.getDate() +
                    "\n Heure : " + realDate.getHours() +
                    "\n Minute : " + realDate.getMinutes() +
                    "\n Seconde : " + realDate.getSeconds() +
                    "\n Décalage Horaire : " + realDate.getTimezoneOffset();
            }
            else if (typeof infosCellule.content == "boolean") {
                var imageBoolean = new Image();
                if (infosCellule.content == true) {
                    imageBoolean.src = '../test/images/true.png';
                }
                else {
                    imageBoolean.src = '../test/images/false.png';
                }

                imageBoolean.classList.add('img');
                cellule.appendChild(imageBoolean);
                cellule.classList.add('img');
            }
            else {
                cellule.innerText = infosCellule.content;
            }

            cellule.setAttribute('id', infosCellule.id);
            cellule.classList.add('cellule');
            cellule.setAttribute("contenteditable", true);
            maLigne.appendChild(cellule);

            var allCellules = document.getElementsByClassName("cellule");
        }
        tableBody.appendChild(maLigne);
    }
    tableau.appendChild(tableBody);
    filtre();

    // Event listeners sur modif manuel du tableau

    var modifierValue = function (event) {
        var id = event.srcElement.id;
        var div = document.getElementById(id)
        var newValue = div.innerText;
        listeModif[id] = newValue;
        sessionStorage.setItem(id, newValue);
    };

    for (var i = 0; i < allCellules.length; i++) {
        allCellules[i].addEventListener("input", modifierValue);
    }

    return Promise.resolve(data);
}


// ---------------------- Autres fonctions


// ---------------------- Event listener sur le fichier json

document.getElementById("filename").addEventListener("input", function () {
    var newInput = document.getElementById("filename").value;
    triEnCours = "aucun";
    clearFormulaireAddLigne();
    listeModif = [];
    sessionStorage.clear();
    getDatas(newInput);

});

// ---------------------- Event listeners sur l'input de filtre

function filtre() {
    var newInput = document.getElementById("filtre").value;

    // code pour le filtre qui cache ou affiche 
    var lignes = tableau.getElementsByClassName('ligne');
    if (newInput == "") {
        for (i = 0; i < lignes.length; i++) {
            var ligne = lignes[i];
            ligne.classList.remove('ligneCaché');
        }
    }

    else {
        for (i = 0; i < lignes.length; i++) {
            var ligne = lignes[i];
            var elementTrouvé = false;
            var cellules = ligne.getElementsByClassName('cellule');


            for (j = 0; j < cellules.length; j++) {
                var cellule = cellules[j];
                if (cellule.innerText.includes(newInput)) {
                    elementTrouvé = true;
                }
            }

            if (elementTrouvé == false) {
                ligne.classList.add('ligneCaché');
            }
            else {
                ligne.classList.remove('ligneCaché');
            }
        }
    }

};
document.getElementById("filtre").addEventListener("input", filtre)



// ---------------------- Event listeners sur les colonnes pour trier par valeur de la colonne

var tri = function (event) {
    // si pas de tri, on check avec l'id du bouton et pas l'inner text
    if (event.srcElement.id == "triReset") {
        triEnCours = "aucun"
        getDatas(document.getElementById("filename").value);
    }

    else {
        triEnCours = event.srcElement.innerText;
        getDatas(document.getElementById("filename").value);
    }
}

// ---------------------- Event listeners sur les boutons et inputs d'ajout de lignes + creation de ceux ci
var boutonAddLigne = function (event) {
    clearFormulaireAddLigne();
    var formulaire = document.getElementById("formulaireAddLigne");
    var container = document.getElementById("inputNewLines");
    container.style.border = "solid black 2px";
    var instruction = document.createElement('h3');
    instruction.innerText = "Veuillez remplir tout les champs";
    container.appendChild(instruction);

    var colonnes = document.getElementById("ligneHeader").children;
    for (colonne of colonnes) {
        var newLabel = document.createElement('label');
        newLabel.innerText = colonne.innerText;
        var newInput = document.createElement('input');
        newInput.type = "text";
        newInput.id = colonne.innerText + "NewLine";
        newInput.required = true;
        newInput.className = "addNewLine";
        container.appendChild(newLabel);
        container.appendChild(newInput);
    }

    var validation = document.createElement('button');
    validation.id = "validationAddLigne"
    validation.innerText = "Valider"
    validation.addEventListener("click", validationAddLigne);
    container.appendChild(validation);
};


function clearFormulaireAddLigne() {
    var container = document.getElementById("inputNewLines");
    container.style.border = null;
    while (container.firstChild) {
        container.firstChild.remove()

    }
    if (document.getElementById("validationAddLigne")) {
        document.getElementById("validationAddLigne").remove();
    }
}

var validationAddLigne = function (event) {
    var inputsNewLine = document.getElementsByClassName("addNewLine");
    for (var i = 0; i < inputsNewLine.length; i++) {
        if (!inputsNewLine[i].value) {
            alert("Vous avez laisser une case vide, heureusement que je m'en suis aperçu !");
            return;
        }
    }

    addNewLineForm = true;
    getDatas(document.getElementById("filename").value);
};

// ---------------------- Appel des events listener et code de l'envoi du JSON
document.getElementById("triReset").addEventListener("click", tri);
document.getElementById("boutonAddLigne").addEventListener("click", boutonAddLigne);



// Envoi au fichier JSON
document.getElementById("jsonSend").addEventListener("click", async function () {

    const loadingresponse = await fetch(document.getElementById("filename").value);
    var data = await getDatas(document.getElementById("filename").value);
    let sendresponse = await fetch('../test/up.php', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });

    let result = await sendresponse.json();
    console.log(result);


});

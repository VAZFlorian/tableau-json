# Guide d'utilisation


## Installation

Telecharger le répo et lancer le fichier index.html situé dans le dossier test.


## Charger un fichier JSON

Commencer par entrer le chemin vers le fichier JSON que vous souhaitez ouvrir.
Il est recommandé de placer vos fichiers dans le dossier data et d'indiquer le chemin en suivant l'exemple des fichiers test affichés à droite de la page.

Si vous souhaitez passer par une **url** vous devrez **autoriser les requetes multiorigines** ( https://pastebin.com/raw/FHHQc4Q7 ne marchera pas en local par défaut )
Il en sera également de même si par la suite vous souhaitez uploader des images et que ces dernières s'affiche.

## Tri des données

Si vous cliquer sur l'en-tête d'une colonne, le tableau sera trié suivant cette dernière.
Par défaut, le tri se fait 
- valeurs true d'abord puis valeurs false dans le cas de **booléen** 
- par nombre croissant d'élements dans le cas d'une liste d'élement (type **Array**)
- dans l'ordre alphanumérique pour les **chaines de caractères** ou les **nombres**
- aucun tri dans le cas ou la donnée ne ressemble à aucun des cas ci-dessus

Le test du type de donnée se fait sur la **valeur de la première ligne** du fichier JSON


## Option de filtre

Rentrer des caracters et le filtre se chargera de n'afficher que les lignes contenant ces caractères.


## Ajout de données 

Cliquer sur le bouton rouge "Ajouter une nouvelle ligne".
Si vous avez bien un tableau d'affiché, vous aurez une case input qui va apparaitre pour chaque colonne du tableau.
Une fois vos données rentrée dans le formulaire cliquer sur le bouton Valider en dessous et le tour est joué.
La nouvelle ligne s'affichera **à la place ou le tri actuel** la placera.
Si vous laissez une case vide, un pop-up vous demandera de remplir tout les champs avant d'envoyer la ligne.

**Note importante : Certaines modifications peuvent être reportées sur d'autres lignes aprés retri du tableau, vous pouvez trier le tableau avant mais pas après avoir modifier des données sous peine d'avoir ces problèmes.**


## Modification de données

Cliquer sur une case, puis modifier les données à votre bon désir
Notez que certaines données sont affiché differement qu'elles sont présente dans la base de données.
Il vous faudra donc écrire dans la cellule la valeur telle que celle çi doit être envoyer dans la base de données.

**Exemple** : Mettre l'url de l'image et non pas l'image en tant que tel


## Envoie des données

Cliquer tout simplement sur le bouton vert "Upload JSON to up.php"

Les données du fichier originel seront recuperé, puis l'ensemble des modifications faite (que ce soit en éditant à la main des cellule ou en ayant ajouté une ligne)
seront re-executé sur les données originel, avant enfin d'être envoyer au fichier up.php. Ce dernier renvoie les informations qu'on lui à envoyer au fichier javascript qui affichera ces dernières dans la console.


## Origine du projet

https://gitlab.com/Nasyourte/exo_editable
